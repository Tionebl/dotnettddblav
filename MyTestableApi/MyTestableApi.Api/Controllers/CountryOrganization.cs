using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyTestableApi.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CountryOrganizationController : ControllerBase
    {
        private readonly ILogger<CountryOrganizationController> _logger;

        public CountryOrganizationController(ILogger<CountryOrganizationController> logger)
        {
            _logger = logger;
        }

        [HttpGet("{country}")]
        public IActionResult Get(string Country)
        {
            List<CountryOrganization> countryList = new()
            {
                new CountryOrganization { Country = "France", Org = new List<string> { "ONU", "NATO" } },
                new CountryOrganization { Country = "Allemagne", Org = new List<string> { "" } },
            };

            var countrySearched = countryList.FirstOrDefault(x => x.Country == Country);

            if (countrySearched == null)
            {
                return NotFound("Ce pays n'existe pas");
            }
            return Ok(countrySearched);
        }
    }
    
    public class CountryOrganization
    {
        public string? Name { get; set; }
        public string? Country { get; set; }
        public List<string>? Org { get; set; }
    }
}
