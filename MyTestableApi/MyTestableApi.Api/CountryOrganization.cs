namespace MyTestableApi.Api;
public class CountryOrganization
{
    public List<string>? Org { get; set; }
    public string? Country { get; set; }
}