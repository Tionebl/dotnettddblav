using Microsoft.AspNetCore.Builder;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Net;
using Newtonsoft.Json;

namespace MyTestableApi.Test;

public class CountryOrganizationTests
{

// Scenario: 1
// Features: Orga inter
// Scenario: OK
// Given le pays que j'ai demandé appartient à un orga inter ex: France 
// when je demande la liste des orga inter " ONU, OTAN, UE"
// then je reçois la liste des orga inter (JSON)
     [Fact]
    public async Task GetOrganizationByCountry()
    {
    
        var country = "France";
        var api = "http://localhost:5288";
        var requestUrl = $"{api}/CountryOrganization/{country}";
        await using var _factory = new WebApplicationFactory<Program>();
        var _client = _factory.CreateClient();

        var response = await _client.GetAsync(requestUrl);
        response.EnsureSuccessStatusCode();

        var content = await response.Content.ReadAsStringAsync();

        Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        Assert.Contains("\"org\":", content);
       
    }
    //scenario: 2
    //given le pays que j'ai demandé n'appartient pas à un orga inter ex: suisse 
    //when je demande la liste des orga inter
    // then je reçois la liste des organisations du pays mais celle-ci est vide

     [Fact]
     public async Task CountryHasNoOrg()
     {
        var country = "Allemagne";// fake exemple
        var api = "http://localhost:5288";
        var requestUrl = $"{api}/CountryOrganization/{country}";
        await using var _factory = new WebApplicationFactory<Program>();
        var _client = _factory.CreateClient();

        var response = await _client.GetAsync(requestUrl);
        response.EnsureSuccessStatusCode();

        var content = await response.Content.ReadAsStringAsync();
        Assert.Contains("\"org\":[\"\"]", content);


     }

        //scenario: 3 donnée d'entrée invalide (cas ou pays n'existe pas)
        //given le pays que j'ai demandé n'existe pas ex: suisse
        //when je demande la liste des orga inter
        // then je reçois l'erreur Http et un json indiquant que le pays n'existe pas
     [Fact]
     public async Task CountryNotExist()
     {
        var country = "Bibiland";// fake country
        var api = "http://localhost:5288";
        var requestUrl = $"{api}/CountryOrganization/{country}";
        await using var _factory = new WebApplicationFactory<Program>();
        var _client = _factory.CreateClient();

        var response = await _client.GetAsync(requestUrl);

        Assert.Equal(HttpStatusCode.NotFound , response.StatusCode);
        var content = await response.Content.ReadAsStringAsync();
        Assert.Contains("Ce pays n'existe pas", content);
     }

}
